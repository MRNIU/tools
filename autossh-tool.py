#!/bin/zsh
# https://github.com/MRNIU

import sys
import os
import argparse
import subprocess

def main():
    arg=get_argv()
    count=arg.count
    port=arg.port
    ip=arg.address
    if not ip:
        print("Please enter ip!")
        exit()

    create_link(ip, count, port)

def get_argv():
    # Create ArgumentParser() object
    parser = argparse.ArgumentParser()

    # Add argument
    parser.add_argument("address",  help="user@host.", type=str, nargs='?')
    parser.add_argument("-n", "--count", help="count of links.", type=int, nargs='?',default=1)
    parser.add_argument("-p", "--port", help="port.", type=int, nargs='?', default=233)


    # Parse argument
    args = parser.parse_args()
    return args

def create_link(host, count, port):
    comd=create_comd(host, count, port)
    if count==1:
        print(comd)
        subprocess.call(comd, shell=True)
    elif count > 1:
        for i in range(count):
            print(comd[i])
            #os.system(comd[i])


def create_comd(host, count, port=233):
    comd="autossh "
    if count==1:
        comd+=  "-M"+ ' '+ str(port)+ ' '+ ''.join(host)
        return comd
    else:
        comds=[]
        for i in range(count):
            comds.append(comd+ "-M"+ ' '+ str((port+i))+ ' '+ ''.join(host))
        return comds


if __name__ == '__main__':
    main()